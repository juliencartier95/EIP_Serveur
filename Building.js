var Requete = require('./Requete')
var app = require('./app')

function BuildBegin(data, CE) {
  var infos = data.split(/;|\n/);
  var typebuilding = infos[0];
  var PosX = infos[1];
  var PosY = infos[2];
  // var Direction = infos[3];
  var url = CE.config.api.BUILDING;
  console.log("ID USER = " + CE.UID);
  var body = JSON.stringify({
    "name": "tmp", "coord_x": PosX, "coord_y": PosY,
    "orientation": 'N', "owner": CE.UID, "type": typebuilding, "enconstruction": 'Construction'
  });
  console.log("Construction = " + url);
  Requete.Requete([url, body], "POST", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      console.log("550" + array._id);
      CE.socket.write("550" + array._id);
    }
    else {
      CE.socket.write("551" + "Error");
      console.log(body);
    }
  })
};

function BuildEnd(data, CE) {
  var infos = data.split(/;|\n/);
  var idBuilding = infos[0];
  var url = CE.config.api.BUILDING + "/" + idBuilding;
  var body = JSON.stringify({
    "enconstruction": 'Finis'
  });
  console.log("Construction finis = " + url);
  Requete.Requete([url, body], "PUT", CE, function (response, body) {
    if (response.statusCode == 200)
      CE.socket.write("552");
    else
      CE.socket.write("553");
  })
};

function UpgradeBuilding(data, CE) {
  var infos = data.split(/;|\n/);
  var idBuilding = infos[0];
  var level = infos[1];
  var url = CE.config.api.BUILDING + "/" + idBuilding;
  var body = JSON.stringify({
    "level": level
  });
  Requete.Requete([url, body], "PUT", CE, function (response, body) {
    if (response.statusCode == 200)
      CE.socket.write("554");
    else
      CE.socket.write("555");
  })
};

function GetBuildingPrice(data, CE) {
  var info = data.split(/;|\n/);
  var type = info[0];
  var level = info[1];
  var url = CE.config.api.GETBUILDINGPRICE;
  var body = JSON.stringify({
    "type": type, "level": level
  });
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200)
      CE.socket.write("556");
    else
      CE.socket.write("557");
  })
};

function DestroyBuilding(data, CE) {
  var info = data.split(/;|\n/);
  var idBuilding = info[0];
  console.log(data);
  var url = CE.config.api.BUILDING + "/" + idBuilding;
  var body = JSON.stringify({});
  console.log("DESTRUCTION = " + url);
  Requete.Requete([url, body], "DELETE", CE, function (response, body) {
    if (response.statusCode == 200)
      CE.socket.write("558");
    else {
      console.log(body);
      CE.socket.write("559" + body);
    }
  })
};

function AddTypeBuilding(data, CE) {
  var infos = data.split(/;|\n/);
  var name = infos[0];
  var length = infos[1];
  var width = infos[2];
  var levelMax = infos[3];
  var wood = infos[4];
  var rock = infos[5];
  var url = CE.config.api.TYPEBUILDING;
  var body = JSON.stringify({
    "name": name,
    "length": length,
    "width": width,
    "levelMax": levelMax,
    "wood": wood,
    "rock": rock
  });
  Requete.Requete([url, body], "POST", CE, function (response, body) {
    if (response.statusCode == 200) {
      console.log("564");
      CE.socket.write("564");
    }
    else {
      CE.socket.write("565");
      console.log(body);
    }
  })
}

function GetTypeBuilding(data, CE) {
  var url = CE.config.api.TYPEBUILDING;
  var body = JSON.stringify({});
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      CE.socket.write("566" + body);
    }
    else {
      CE.socket.write("567");
      console.log(body);
    }
  })
}

exports.BuildBegin = BuildBegin;
exports.BuildEnd = BuildEnd;
exports.UpgradeBuilding = UpgradeBuilding;
exports.GetBuildingPrice = GetBuildingPrice;
exports.DestroyBuilding = DestroyBuilding;
exports.AddTypeBuilding = AddTypeBuilding;
exports.GetTypeBuilding = GetTypeBuilding;