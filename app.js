
var net = require('net');
var Transform = require('stream').Transform;
var request = require('request');
var ini = require('ini');
var fs = require('fs');
var Connect = require('./Connexion');
var Building = require('./Building');
var Fight = require('./Fight');
var Ressource = require('./Ressource');
var Inventory = require('./Inventory');
var Champions = require('./Champions');
var Team = require('./Team');

var file = fs.readFileSync('./config.ini', 'utf-8');
var config = ini.parse(file);

var HOST = config.reseau.HOST;
var PORT = config.reseau.PORT;

function ClassEssentiel() {
    var socket;
    var config;
    var Token;
    var UID;
};

module.exports.ClassEssentiel = ClassEssentiel;

var server = net.createServer(function (socket) {
    var UID;
    var Token;

    console.log("connected");
    socket.on("error", function (err) {
        console.log("Caught flash policy server socket error: ");
        console.log(err.stack);
    });
    socket.on('data', function (data) {
        console.log(data.toString());
        ClassEssentiel.socket = socket;
        ClassEssentiel.config = config;
        ClassEssentiel.Token = Token;
        ClassEssentiel.UID = UID;
        console.log("------- " + ClassEssentiel.UID);
        readCmd(data.toString(), ClassEssentiel, function (ID, token) {
            UID = ID;
            Token = token;
        });
    })
    socket.on('close', function () {
        console.log("CLOSED");
    })
}).listen(PORT, function () {
    console.log('server listening to %j', server.address());
});


var readCmd = function (data, ClassEssentiel, callback) {
    var tmp = data[0];
    for (var i = 1; i < 3; i++)
        tmp += data[i];
    var cmd = parseInt(tmp.toString());
    // var rep = [,];
    var param = data[3];
    for (var i = 4; i < data.length; i++)
        param += data[i];
    console.log(data);
    switch (cmd) {
        //Connexion
        case 100:
            console.log('handshake');
            Connect.Handshake(ClassEssentiel);
            break;
        case 101:
            console.log('Subscribe');
            Connect.Subscribe(param, ClassEssentiel);
            break;
        case 102:
            console.log('Login');
            Connect.Login(param, ClassEssentiel, function (ID, Token) { callback(ID, Token, null); });
            break;
        case 103:
            console.log('LoadGames');
            Connect.LoadGames(param, ClassEssentiel, function (IDCity) { callback(ClassEssentiel.ID, ClassEssentiel.Token, IDCity) });
            break;
        case 104:
            console.log('NewGames');
            Connect.NewGames(ClassEssentiel);
            break;

        //Ressources
        case 110:
            console.log('GetRessources');
            Ressource.GetRessources(param, ClassEssentiel);
            break;
        case 111:
            console.log("AddRock");
            Ressource.AddRock(param, ClassEssentiel);
            break;
        case 112:
            console.log("AddWood");
            Ressource.AddWood(param, ClassEssentiel);
            break;
        case 113:
            console.log("AddGold");
            Ressource.AddGold(param, ClassEssentiel);
            break;
        case 120:
            console.log("GetExchangeRate");
            Ressource.GetExchangeRate(ClassEssentiel);
            break;
        case 121:
            console.log("RemoveRock");
            Ressource.RemoveRock(param, ClassEssentiel);
            break;
        case 122:
            console.log("RemoveWood");
            Ressource.RemoveWood(param, ClassEssentiel);
            break;
        case 123:
            console.log("RemoveGold");
            Ressource.RemoveGold(param, ClassEssentiel);
            break;
        case 126:
            console.log("Gathering");
            Ressource.Gathering(param, ClassEssentiel);
            break;
        case 127:
            console.log("StopGathering");
            Ressource.StopGathering(param, ClassEssentiel);
            break;

        //Buildings
        case 131:
            console.log('BuildBegin ' + ClassEssentiel.UID);
            Building.BuildBegin(param, ClassEssentiel);
            break;
        case 132:
            console.log('BuildEnd');
            Building.BuildEnd(param, ClassEssentiel);
            break;
        case 133:
            console.log('UpgradeBuilding');
            Building.UpgradeBuilding(param, ClassEssentiel);
            break;
        case 134:
            console.log('GetBuildingPrice');
            Building.GetBuildingPrice(param, ClassEssentiel);
            break;
        case 135:
            console.log('DestroyBuilding');
            Building.DestroyBuilding(param, ClassEssentiel);
            break;
        case 136:
            console.log('AddTypeBuilding');
            Building.AddTypeBuilding(param, ClassEssentiel);
            break;
        case 137:
            console.log('GetTypeBuilding');
            Building.GetTypeBuilding(param, ClassEssentiel);
            break;


        //Champions
        case 151:
            console.log('AddChampion');
            Champions.AddChampion(param, ClassEssentiel);
            break;
        case 152:
            console.log('RemoveChampion');
            Champions.RemoveChampion(param, ClassEssentiel);
            break;
        case 153:
            console.log('GetChampion');
            Champions.GetChampions(param, ClassEssentiel);
            break;
        case 154:
            console.log("GetallChampion");
            Champions.GetAllChampions(param, ClassEssentiel);
            break;
        case 155:
            console.log("AddClass");
            Champions.AddClass(param, ClassEssentiel);
            break;
        case 156:
            console.log("GetClass");
            Champions.GetClass(param, ClassEssentiel);
            break;
       /* case 157:
            console.log("AddStuffHand");
            Champions.AddStuffHand(param, ClassEssentiel);
            break;
        case 158:
            console.log("AddStuffBody");
            Champions.AddStuffBody(param, ClassEssentiel);
            break;
        case 159:
            console.log("AddStuffHead");
            Champions.AddStuffHead(param, ClassEssentiel);
            break;
        case 160:
            console.log("AddStuffLegs");
            Champions.AddStuffHead(param, ClassEssentiel);
            break;
        case 161:
            console.log("AddStuffFeet");
            Champions.AddStuffHead(param, ClassEssentiel);
            break;
        case 162:
            console.log("AddStuffGloves");
            Champions.AddStuffHead(param, ClassEssentiel);
            break;
        case 163:
            console.log("RemoveStuffHand");
            Champions.RemoveStuffHand(param, ClassEssentiel);
            break;
        case 164:
            console.log("RemoveStuffBody");
            Champions.RemoveStuffBody(param, ClassEssentiel);
            break;
        case 165:
            console.log("RemoveStuffHead");
            Champions.RemoveStuffHead(param, ClassEssentiel);
            break;
        case 166:
            console.log("RemoveStuffLegs");
            Champions.RemoveStuffHead(param, ClassEssentiel);
            break;
        case 167:
            console.log("RemoveStuffFeet");
            Champions.RemoveStuffHead(param, ClassEssentiel);
            break;
        case 168:
            console.log("RemoveStuffGloves");
            Champions.RemoveStuffHead(param, ClassEssentiel);
            break;*/
        case 169:
            console.log("AddStuff");
            Champions.AddStuff(param, ClassEssentiel);
            break;
        case 170:
            console.log("RemoveStuff");
            Champions.RemoveStuff(param, ClassEssentiel);
            break;

        //Combats
        case 201:
            console.log('GetListCombat');
            Fight.GetListCombat(param, ClassEssentiel);
            break;
        case 202:
            console.log('GetCombatContent');
            Fight.GetCombatContent(param, ClassEssentiel);
            break;
        case 203:
            //  console.log('StartFight');
            //Fight.StartFight(param, ClassEssentiel);
            break;
        case 204:
            //console.log('Attack');
            //Fight.Attack(param, ClassEssentiel);
            break;
        case 205:
            //console.log('Magie');
            //Fight.Magie(param, ClassEssentiel);
            break;
        case 206:
            //console.log('Object');
            //Fight.Object(param, ClassEssentiel);
            break;
        case 207:
            //console.log('Pass');
            //Fight.Pass(param, ClassEssentiel);
            break;
        case 208:
            //console.log('Forfeit');
            //Fight.Forfeit(param, ClassEssentiel);
            break;
        case 210:
            console.log('Win');
            Fight.Win(param, ClassEssentiel);
            break;
        case 211:
            console.log('FillListCombats');
            Fight.Fill(param, ClassEssentiel);
            break;

        //Inventory
        case 171:
            console.log('GetInventory');
            Inventory.GetInventory(param, ClassEssentiel);
            break;
        case 172:
            console.log('SellObject');
            Inventory.SellObject(param, ClassEssentiel);
            break;
        case 173:
            console.log("GetInventoryUsable");
            Inventory.GetInventoryUsable(param, ClassEssentiel);
            break;
        case 174:
            console.log("AddItem");
            Inventory.AddItem(param, ClassEssentiel);
            break;
        case 175:
            console.log('UseItem');
            Inventory.UseItem(param, ClassEssentiel);
            break;
        case 176:
            console.log("AddUsable");
            Inventory.AddUsable(param, ClassEssentiel);
            break;
        case 177:
            console.log("UseUsable");
            Inventory.UseUsable(param, ClassEssentiel);
            break;
        case 178:
            console.log("GetTotalInventory");
            Inventory.GetTypeInventory(param, ClassEssentiel);
            break;
        case 179:
            console.log("GetTotalUsable");
            Inventory.GetTypeUsable(param, ClassEssentiel);
            break;

        //TEAM
        case 191:
            console.log("AddToTeam");
            Team.AddToTeam(param, ClassEssentiel);
            break;
        case 192:
            console.log("GetTeams");
            Team.GetTeams(param, ClassEssentiel);
            break;
        case 193:
            console.log("RemoveFromTeam");
            Team.RemoveFromTeam(param, ClassEssentiel);
            break;
        default:
            //console.log('Not implemented');
            // rep = "404";
            break;
    }
    //   socket.write(rep);
    return;
}


exports.readCmd = readCmd;
