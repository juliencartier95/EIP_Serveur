var Requete = require('./Requete')
var app = require('./app')

function GetListCombat(data, CE) {
	var infos = data.split(/;|\n/);
	var url = CE.config.api.TEAM;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var list = JSON.parse(body);
			var array = [];
			var count = 0;
			for (var i = 0, len = list.length; i < len; i++) {
				(function (i) {
					if (!list[i].hasOwnProperty('owner'))
						array.push(list[i]);
					console.log(list[i]);
					count++
					if (count == len) {
						console.log(array);
						CE.socket.write("600" + JSON.stringify(array));
					}
				})(i)
			}
		}
		else
			CE.socket.write("601");
	});
}


function GetCombatContent(data, CE) {
	var infos = data.split(/;|\n/);
	var idFight = infos[0];
	var url = CE.config.api.TEAM + "/" + idFight;
	var body = JSON.stringify({});
	var monstre1_id = "0";
	var monstre2_id = "0";
	var monstre3_id = "0";
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var array = JSON.parse(body);
			if (array) {
				monstre1_id = array.monster_one;
				monstre2_id = array.monster_two;
				monstre3_id = array.monster_three;
			}
			var monstre1;
			url = CE.config.api.MONSTER + "/" + monstre1_id;
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					monstre1 = JSON.parse(body);
					var monstre2;
					url = CE.config.api.MONSTER + "/" + monstre2_id;
					Requete.Requete([url, body], "GET", CE, function (response, body) {
						if (response.statusCode == 200) {
							monstre2 = JSON.parse(body);
							var monstre3;
							url = CE.config.api.MONSTER + "/" + monstre3_id;
							Requete.Requete([url, body], "GET", CE, function (response, body) {
								if (response.statusCode == 200) {
									monstre3 = JSON.parse(body);
									var url = CE.config.api.FIGHT + "?player=" + CE.UID + "&team=" + idFight;
									var body = JSON.stringify({});
									Requete.Requete([url, body], "GET", CE, function (response, body) {
										if (response.statusCode == 200) {
											var array = JSON.parse(body);
											var rank = 0;
											if (array.length >= 1)
												var rank = array[0].rank;
											//var listcombat = JSON.stringify([monstre1, monstre2, monstre3]);
											var Jrank = JSON.stringify({ "rank": rank, "team": [monstre1, monstre2, monstre3] });
											CE.socket.write("602" + Jrank);
										}
									});
								}
								else { CE.socket.write("603"); };
							});
						}
						else { CE.socket.write("603"); };
					});
				}
				else { CE.socket.write("603"); };
			});
		}
		else
			CE.socket.write("603");
	})
}

function StartFight(data, CE) {
	/*var infos = data.split(/;|\n/);
	var idFight = infos[0];
	var ListHero = infos[1];
	var url = CE.config.api.STARTFIGHT;
	var body = JSON.stringify({ "id": idFight, "ListHero": ListHero });
	Requete.Requete([url, body], "POST", CE, function (response, body) {
		if (response.statusCode == 200) {
			var array = JSON.parse(body);
			var idfight = array.id;
			CE.socket.write("604" + idfight);
		}
		else
			CE.socket.write("605" + idFight);
	})*/
}

function Attack(data, CE) {
	/*	var infos = data.split(/;|\n/);
		var idFight = infos[0];
		var idFighter = infos[1];
		var idTarget = infos[2];
		var idTypeAttack = infos[3];
		var url = CE.config.api.ATTACK;
		var body = JSON.stringify({
			"id": idFight, "idFighter": idFighter, "idTarget": idTarget,
			"ïdTypeAttack": idTypeAttack
		});
		Requete.Requete([url, body], "POST", CE, function (response, body) {
			if (response.statusCode == 200) {
				var array = JSON.parse(body);
				var idfight = array.id;
				idTypeAttack = array.Attack;
				idFighter = array.Fighter;
				idTarget = array.Target;
				CE.socket.write("606" + idfight + ';' + idTypeAttack + ';' + idFighter + ';' + idTarget);
			}
			else
				CE.socket.write("607" + idFight);
		})*/
}

function Magie(data, CE) {
	/*var infos = data.split(/;|\n/);
	var idFight = infos[0];
	var idFighter = infos[1];
	var idTarget = infos[2];
	var idTypeMagie = infos[3];
	var url = CE.config.api.MAGIE;
	var body = JSON.stringify({
		"id": idFight, "idFighter": idFighter, "idTarget": idTarget,
		"ïdTypeMagie": idTypeMagie
	});
	Requete.Requete([url, body], "POST", CE, function (response, body) {
		if (response.statusCode == 200) {
			var array = JSON.parse(body);
			var idfight = array.id;
			idTypeMagie = array.Magie;
			idFighter = array.Fighter;
			idTarget = array.Target;
			CE.socket.write("608" + idfight + ';' + idTypeMagie + ';' + idFighter + ';' + idTarget);
		}
		else
			CE.socket.write("609" + idFight);
	})*/
}

function UseObject(data, CE) {
	/*	var infos = data.split(/;|\n/);
		var idFight = infos[0];
		var idFighter = infos[1];
		var idTarget = infos[2];
		var idTypeObjet = infos[3];
		var url = CE.config.api.OBJET;
		var body = JSON.stringify({
			"id": idFight, "idFighter": idFighter, "idTarget": idTarget,
			"ïdTypeObjet": idTypeObjet
		});
		Requete.Requete([url, body], "POST", CE, function (response, body) {
			if (response.statusCode == 200) {
				var array = JSON.parse(body);
				var idfight = array.id;
				idTypeObjet = array.Objet;
				idFighter = array.Fighter;
				idTarget = array.Target;
				CE.socket.write("610" + idfight + ';' + idTypeObjet + ';' + idFighter + ';' + idTarget);
			}
			else
				CE.socket.write("611" + idFight);
		})*/
}

function Pass(data, CE) {
	/*	var idFight = data;
		var url = CE.config.api.PASS;
		var body = JSON.stringify({ "id": idFight });
		Requete.Requete([url, body], "POST", CE, function (response, body) {
			if (response.statusCode == 200)
				CE.socket.write("612");
			else
				CE.socket.write("613");
		})*/
}

function Forfeit(data, CE) {
	/*	var idFight = data;
		var url = CE.config.api.FORFEIT;
		var body = JSON.stringify({ "id": idFight });
		Requete.Requete([url, body], "POST", CE, function (response, body) {
			if (response.statusCode == 200)
				CE.socket.write("614");
			else
				CE.socket.write("615");
		})*/
}

function Win(data, CE) {
	var infos = data.split(/;|\n/);
	var idCombat = infos[0];
	var rank = infos[1];
	var url = CE.config.api.TEAM + "/" + idCombat;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var array = JSON.parse(body);
			var reward = parseInt(array.reward);
			url = CE.config.api.CITY + "?owner=" + CE.UID;
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var array = JSON.parse(body);
					var gold = parseInt(array[0].gold);
					var idcity = array[0]._id;
					url = CE.config.api.CITY + "/" + idcity;
					reward += gold;
					body = JSON.stringify({
						"gold": reward
					});
					Requete.Requete([url, body], "PUT", CE, function (response, body) {
						if (response.statusCode == 200) {
							url = CE.config.api.FIGHT;
							var url2 = url + "?player=" + CE.UID + "&team=" + idCombat;
							console.log(url2);
							body = JSON.stringify({});
							Requete.Requete([url2, body], "GET", CE, function (response, body) {
								if (response.statusCode == 200) {
									var array = JSON.parse(body);
									var ExistingRank = array[0].rank;
									if (array.length < 1) {
										body = JSON.stringify({ "team": idCombat, "player": CE.UID, "rank": rank });
										Requete.Requete([url, body], "POST", CE, function (response, body) {
											if (response.statusCode == 200) {
												console.log("body = " + body);
												CE.socket.write("618");
											}
											else { CE.socket.write("619") };
										});
									}
									else {
										if (ExistingRank >= rank) {
											CE.socket.write("618");
										}
										else {
											var idfight = array[0]._id;
											url = url + "/" + idfight;
											body = JSON.stringify({ "rank": rank });
											Requete.Requete([url, body], "PUT", CE, function (response, body) {
												if (response.statusCode == 200) {
													CE.socket.write("618");
												}
												else { CE.socket.write("619") };
											});
										}
									}
								}
								else { CE.socket.write("619") };
							});
						}
						else
							CE.socket.write("619");
					});
				}
				else
					CE.socket.write("619");
			});
		}
		else { CE.socket.write("619") };
	});
}

function Fill(data, CE) {
	var infos = data.split(/;|\n/);
	var nbcombat = infos[0];
	var id1 = infos[1];
	var id2 = infos[2];
	var id3 = infos[3];
	var monster1;
	var monster2;
	var monster3;
	var url = CE.config.api.MONSTER + "/" + id1;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var array = JSON.parse(body);
			monster1 = array;
			var url = CE.config.api.MONSTER + "/" + id2;
			body = JSON.stringify({});
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var array = JSON.parse(body);
					monster2 = array;
					var url = CE.config.api.MONSTER + "/" + id3;
					body = JSON.stringify({});
					Requete.Requete([url, body], "GET", CE, function (response, body) {
						if (response.statusCode == 200) {
							var array = JSON.parse(body);
							monster3 = array;
							console.log(monster1);
							console.log(monster2);
							console.log(monster3);
							var url = CE.config.api.TEAM;
							body = JSON.stringify({ "name": nbcombat, "monster_one": monster1, "monster_two": monster2, "monster_three": monster3 });
							Requete.Requete([url, body], "POST", CE, function (response, body) {
								if (response.statusCode == 200) {
									console.log(body);
									CE.socket.write("620");
								}
								else {
									console.log(body);
									CE.socket.write("621");
								}
							});
						}
					});
				}
			});
		}
	});

}

exports.GetListCombat = GetListCombat;
exports.GetCombatContent = GetCombatContent;
exports.StartFight = StartFight;
exports.Attack = Attack;
exports.Magie = Magie;
exports.Object = UseObject;
exports.Pass = Pass;
exports.Forfeit = Forfeit;
exports.Win = Win;
exports.Fill = Fill;