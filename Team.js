var Requete = require('./Requete')
var app = require('./app')

function AddToTeam(data, CE) {
    var infos = data.split(/;|\n/);
    var ChampionID = infos[0];
    var Position = infos[1];

    var url = CE.config.api.TEAM;
    var body = JSON.stringify({});
    Requete.Requete([url + "?owner=" + CE.UID, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            var array = JSON.parse(body);
            if (array.length <= 0) {
                var url = CE.config.api.TEAM;
                body = JSON.stringify({ "owner": CE.UID, "name": CE.UID, "champion_one": null, "champion_two": null, "champion_three": null });
                console.log(url + " == " + body);
                Requete.Requete([url, body], "POST", CE, function (response, body) {
                    if (response.statusCode != 200)
                        CE.socket.write("921");
                    else
                        AddToTeam(data, CE);
                });
            }
            else {
                if (Position == 1)
                    body = JSON.stringify({ "champion_one": ChampionID });
                else if (Position == 2)
                    body = JSON.stringify({ "champion_two": ChampionID });
                else if (Position == 3)
                    body = JSON.stringify({ "champion_three": ChampionID });
                var url = CE.config.api.TEAM;
                console.log(url + "/" + array[0]._id + " ==== " + body);
                Requete.Requete([url + "/" + array[0]._id, body], "PUT", CE, function (response, body) {
                    console.log(body);
                    console.log(response);
                    if (response.statusCode == 200) {
                        CE.socket.write("920");
                    }
                    else
                        CE.socket.write("921");
                })
            }
        }
    })
}

function GetTeams(data, CE) {
    var infos = data.split(/;|\n/);
    var url = CE.config.api.TEAM;
    var body = JSON.stringify({});
    Requete.Requete([url + "?owner=" + CE.UID, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            var array = JSON.parse(body);
            if (array.length <= 0) {
                var url = CE.config.api.TEAM;
                body = JSON.stringify({ "owner": CE.UID, "name": CE.UID, "champion_one": null, "champion_two": null, "champion_three": null });
                console.log(url + " == " + body);
                Requete.Requete([url, body], "POST", CE, function (response, body) {
                    if (response.statusCode != 200)
                        CE.socket.write("921");
                    else
                        GetTeams(data, CE);
                });
            }
            else {
                if (array[0].champion_one == null)
                    var champion_one = "";
                else
                    var champion_one = array[0].champion_one
                if (array[0].champion_two == null)
                    var champion_two = "";
                else
                    var champion_two = array[0].champion_two
                if (array[0].champion_three == null)
                    var champion_three = "";
                else
                    var champion_three = array[0].champion_three
                body = JSON.stringify([{ "champion_one": champion_one, "champion_two": champion_two, "champion_three": champion_three }]);
                CE.socket.write("922" + body);
            }
        } else {
            console.log("Casséee");
            CE.socket.write("923");
        }
    });
}

function RemoveFromTeam(data, CE) {
    console.log("Remove FROM TEAM");
    var infos = data.split(/;|\n/);
    var Position = infos[0];
    var url = CE.config.api.TEAM;
    var body = JSON.stringify({});
    Requete.Requete([url + "?owner=" + CE.UID, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            var array = JSON.parse(body);
            if (array.length <= 0) {
                console.log("Error array" + body);
                CE.socket.write("925");
            }
            else {
                if (Position == 1) {
                    console.log("Remove 1");
                    var body2 = JSON.stringify({ "champion_one": null });
                    Requete.Requete([url + "/" + array[0]._id, body2], "PUT", CE, function (response, body2) {
                        if (response.statusCode = 200) {
                            CE.socket.write("924");
                        }
                        else
                            CE.socket.write("925");
                    });
                }
                else if (Position == 2) {
                    console.log("Remove 2");
                    var body2 = JSON.stringify({ "champion_two": null });
                    Requete.Requete([url + "/" + array[0]._id, body2], "PUT", CE, function (response, body2) {
                        if (response.statusCode = 200) {
                            CE.socket.write("924");
                        }
                        else
                            CE.socket.write("925");
                    });
                }
                else if (Position == 3) {
                    console.log("Remove 3");
                    var body2 = JSON.stringify({ "champion_three": null });
                    console.log(url + "/" + array[0]._id + " === " + body2);
                    Requete.Requete([url + "/" + array[0]._id, body2], "PUT", CE, function (response, body2) {
                        if (response.statusCode = 200) {
                            CE.socket.write("924");
                        }
                        else
                            CE.socket.write("925");
                    });
                }
                else
                {
                    console.log("Erreur de position");
                    CE.socket.write("925");
                }
            }
        }
        else
            CE.socket.write("925");
    });
}

exports.AddToTeam = AddToTeam;
exports.GetTeams = GetTeams;
exports.RemoveFromTeam = RemoveFromTeam;