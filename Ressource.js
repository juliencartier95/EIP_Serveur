var Requete = require('./Requete')
var app = require('./app')

function GetRessources(data, CE) {
  var url = CE.config.api.CITY + "?owner=" + CE.UID;
  var body = JSON.stringify({});
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      if (array[0].hasOwnProperty('rock') && array[0].hasOwnProperty('wood') && array[0].hasOwnProperty('gold')) {
        var rock = array[0].rock;
        var wood = array[0].wood;
        var gold = array[0].gold;
        CE.socket.write("700" + rock + ";" + wood + ";" + gold);
      }
      else
        CE.socket.write("701");
    }
    else
      CE.socket.write("701");
  })
};

function AddRock(data, CE) {
  var infos = data.split(/;|\n/);
  var quantity = parseInt(infos[0]);
  var body = JSON.stringify({});
  url = CE.config.api.CITY + "?owner=" + CE.UID;
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var rock = parseInt(array[0].rock);
      var idcity = array[0]._id;
      url = CE.config.api.CITY + "/" + idcity;
      quantity += rock;
      console.log(quantity);
      body = JSON.stringify({
        "rock": quantity
      });
      Requete.Requete([url, body], "PUT", CE, function (response, body) {
        if (response.statusCode == 200)
          CE.socket.write("702");
        else
          CE.socket.write("703");
      });
    }
  });
}

function AddWood(data, CE) {
  var infos = data.split(/;|\n/);
  var quantity = parseInt(infos[0]);
  var body = JSON.stringify({});
  url = CE.config.api.CITY + "?owner=" + CE.UID;
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var wood = parseInt(array[0].wood);
      var idcity = array[0]._id;
      url = CE.config.api.CITY + "/" + idcity;
      quantity += wood;
      console.log(quantity);
      body = JSON.stringify({
        "wood": quantity
      });
      Requete.Requete([url, body], "PUT", CE, function (response, body) {
        if (response.statusCode == 200)
          CE.socket.write("704");
        else
          CE.socket.write("705");
      });
    }
  });
}

function AddGold(data, CE) {
  var infos = data.split(/;|\n/);
  var quantity = parseInt(infos[0]);
  var body = JSON.stringify({});
  url = CE.config.api.CITY + "?owner=" + CE.UID;
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var gold = parseInt(array[0].gold);
      var idcity = array[0]._id;
      url = CE.config.api.CITY + "/" + idcity;
      quantity += gold;
      console.log(quantity);
      body = JSON.stringify({
        "gold": quantity
      });
      Requete.Requete([url, body], "PUT", CE, function (response, body) {
        if (response.statusCode == 200)
          CE.socket.write("706");
        else
          CE.socket.write("707");
      });
    }
  });
}



function GetExchangeRate(data, CE) {
  var url = CE.config.api.GETEXCHANGERATE;
  var body = JSON.stringify({});
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var Ore = array.Ore_rate;
      var Wood = array.Wood_rate;
      CE.socketwrite("708" + Ore + ";" + Wood);
    }
    else
      CE.socketwrite("709");
  })
}

function RemoveRock(data, CE) {
  var infos = data.split(/;|\n/);
  var quantity = parseInt(infos[0]);
  var body = JSON.stringify({});
  url = CE.config.api.CITY + "?owner=" + CE.UID;
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var rock = parseInt(array[0].rock);
      var idcity = array[0]._id;
      url = CE.config.api.CITY + "/" + idcity;
      quantity = rock - quantity;
      if (quantity >= 0) {
        console.log(quantity);
        body = JSON.stringify({
          "rock": quantity
        });
        Requete.Requete([url, body], "PUT", CE, function (response, body) {
          if (response.statusCode == 200)
            CE.socket.write("722");
          else
            CE.socket.write("723");
        });
      }
      else
      CE.socket.write("723" + "Can't be negative");
    }
  });
}

function RemoveWood(data, CE) {
  var infos = data.split(/;|\n/);
  var quantity = parseInt(infos[0]);
  var body = JSON.stringify({});
  url = CE.config.api.CITY + "?owner=" + CE.UID;
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var wood = parseInt(array[0].wood);
      var idcity = array[0]._id;
      url = CE.config.api.CITY + "/" + idcity;
      quantity = wood - quantity;
      if (quantity >= 0) {
        console.log(quantity);
        body = JSON.stringify({
          "wood": quantity
        });
        Requete.Requete([url, body], "PUT", CE, function (response, body) {
          if (response.statusCode == 200)
            CE.socket.write("724");
          else
            CE.socket.write("725");
        });
      }
      else
        CE.socket.write("725" + "Can't be negative");
    }
  });
}

function RemoveGold(data, CE) {
  var infos = data.split(/;|\n/);
  var quantity = parseInt(infos[0]);
  var body = JSON.stringify({});
  url = CE.config.api.CITY + "?owner=" + CE.UID;
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      var gold = parseInt(array[0].gold);
      var idcity = array[0]._id;
      url = CE.config.api.CITY + "/" + idcity;
      quantity = gold - quantity;
      if (quantity >= 0) {
        console.log(quantity);
        body = JSON.stringify({
          "gold": quantity
        });
        Requete.Requete([url, body], "PUT", CE, function (response, body) {
          if (response.statusCode == 200)
            CE.socket.write("726");
          else
            CE.socket.write("727");
        });
      }
      else
        CE.socket.write("727" + "Can't be negative");
    }
  });
}

function Gathering(data, CE) {
  var infos = data.split(/;|\n/);
  var idressource = infos[0];
  var nbpeon = infos[1];
  var url = CE.config.api.GATHERING;
  var body = JSON.stringify({
    "idressource": idressource, "nbpeon": nbpeon
  });
  Requete.Requete([url, body], "POST", CE, function (response, body) {
    if (response.statusCode == 200)
      CE.socket.write("714");
    else
      CE.socket.write("715");
  })
}

exports.GetRessources = GetRessources;
exports.AddRock = AddRock;
exports.AddWood = AddWood;
exports.AddGold = AddGold;
exports.GetExchangeRate = GetExchangeRate;
exports.RemoveRock = RemoveRock;
exports.RemoveWood = RemoveWood;
exports.RemoveGold = RemoveGold;
exports.Gathering = Gathering;
