var app = require('../app')
var assert = require('assert');

describe('app', function () {
  describe('#Handshake', function () {
    it('', function () {
      assert.equal(app.readCmd("100"), 502)
    })
  })
  describe('#Inscription', function () {
    it('', function () {
      assert.equal(app.readCmd("101Julien;TOTO;julien@tyras.com"), 512)
    })
  })
  describe('#Connexion', function () {
    it('', function () {
      assert.equal(app.readCmd("102Julien;TOTO"), 522)
    })
  })
  describe('#ChargerJeu', function () {
    it('', function () {
      assert.equal(app.readCmd("103"), 532)
    })
  })
  describe('#NewGames', function () {
    it('', function () {
      assert.equal(app.readCmd("104"), 542)
    })
  })
  describe('#Build Begin', function () {
    it('', function () {
      assert.equal(app.readCmd("1310;42;42;E"), 552)
    })
  })
  describe('#Build Finish', function () {
    it('', function () {
      assert.equal(app.readCmd("13239456"), 553)
    })
  })
  describe('#Build Upgrade', function () {
    it('', function () {
      assert.equal(app.readCmd("13354654"), 555)
    })
  })
  describe('#Build Price', function () {
    it('', function () {
      assert.equal(app.readCmd("1340;3"), 557)
    })
  })
  describe('#Build Destruction', function () {
    it('', function () {
      assert.equal(app.readCmd("135165156"), 558)
    })
  })
  describe('#Build Destruction Confirm', function () {
    it('', function () {
      assert.equal(app.readCmd("136165156"), 561)
    })
  })
  describe('#Build Destruction Cancel', function () {
    it('', function () {
      assert.equal(app.readCmd("137165156"), 560)
    })
  })
  describe('#Build Move', function () {
    it('', function () {
      assert.equal(app.readCmd("138165156;42;42;E"), 563)
    })
  })
  describe('#Liste Combat', function () {
    it('', function () {
      assert.equal(app.readCmd("201"), 601)
    })
  })
  describe('#CombatContent', function () {
    it('', function () {
      assert.equal(app.readCmd("2026545"), 603)
    })
  })
  describe('#Start Fight', function () {
    it('', function () {
      assert.equal(app.readCmd("2036545;{486:5646}"), 605)
    })
  })
  describe('#Attack', function () {
    it('', function () {
      assert.equal(app.readCmd("2046545;486;5;7"), 607)
    })
  })
  describe('#Magie', function () {
    it('', function () {
      assert.equal(app.readCmd("2056545;486;5;7"), 609)
    })
  })
  describe('#Object', function () {
    it('', function () {
      assert.equal(app.readCmd("2066545;486;5;7"), 611)
    })
  })
  describe('#Pass', function () {
    it('', function () {
      assert.equal(app.readCmd("2076586"), 613)
    })
  })
  describe('#Forfeit', function () {
    it('', function () {
      assert.equal(app.readCmd("2088633"), 615)
    })
  })
  describe('#Update', function () {
    it('', function () {
      assert.equal(app.readCmd("2094639"), 617)
    })
  })
})
