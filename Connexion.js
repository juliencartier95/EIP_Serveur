var http = require('http');
var Requete = require('./Requete');
var app = require('./app');
const querystring = require('querystring');

function Handshake(CE) {
  CE.socket.write("501")
};

function Subscribe(data, CE) {
  var infos = data.split(/;|\n/);
  var login = infos[0];
  var password = infos[1];
  var email = infos[2];
  var url = CE.config.api.SUBSCRIBE;
  console.log("Inscription = " + url);
  var body = JSON.stringify({ "email": email, "password": password });
  Requete.FirstRequete([url, body], CE, function (response, body) {
    if (response.statusCode == 200)
      CE.socket.write("511");
    else
      CE.socket.write("512");
  })
};

function Login(data, CE, callback) {
  var info = data.split(/;|\n/);
  var login = info[0];
  var password = info[1];
  var url = CE.config.api.LOGIN;
  var body = JSON.stringify({ "email": login, "password": password });
  console.log("Login = " + url);
  Requete.FirstRequete([url, body], CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      console.log("ID = " + array.account._id);
      CE.socket.write("521");
      callback(array.account._id, array.token);
    }
    else
      CE.socket.write("522");
  })
};

function LoadGames(data, CE, callback) {
  var info = data.split(/;|\n/);
  var url = CE.config.api.CITY;
  url += "?owner=" + CE.UID;
  var body = JSON.stringify({});
  console.log("Get City = "+ url);
  Requete.Requete([url, body], "GET", CE, function (response, body) {
    if (response.statusCode == 200) {
      var array = JSON.parse(body);
      if (body == "[]") {
        createCity(data, CE, url, callback);
      }
      else {
        array = array[0];
        var wood = array.wood;
        var rock = array.rock;
        var gold = array.gold;
        var id = array._id;
        url = CE.config.api.BUILDING + "?owner=" + CE.UID;
        body = JSON.stringify({});
        console.log("Get Buildings=" + url);
        Requete.Requete([url, body], "GET", CE, function (response, body) {
          if (response.statusCode != 200) {
            CE.socket.write("532");
            return;
          }
          else {
            console.log("Envois des infos = "+ wood + ";" + rock+";" + gold + ";" +body);
            CE.socket.write("531" + wood + ";" + rock+";"+ gold + ";" +body);
            console.log("====== " + CE.UID);
          }
        })
      }
    }
    else {
      CE.socket.write("532");
    }
  })
};

function createCity(data, CE, url, callback) {
  var body = JSON.stringify({ "owner": CE.UID });
  console.log("Creation de la ville = " + url);
  Requete.Requete([url, body], "POST", CE, function (response, body) {
    if (response.statusCode == 200) {
      LoadGames(data, CE, callback);
    }
    else
      CE.socket.write("532");
  })
}

function NewGames(CE) {
  //  var url = 'http://localhost:4000/api/1.0/auth/';
  CE.socket.write("542");
};

exports.Handshake = Handshake;
exports.Subscribe = Subscribe;
exports.Login = Login;
exports.LoadGames = LoadGames;
exports.NewGames = NewGames;
