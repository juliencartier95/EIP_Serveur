var Requete = require('./Requete')
var app = require('./app')
var _ = require('underscore')

function AddChampion(data, CE) {
	var infos = data.split(/;/);
	if (infos.length >= 8) {
		var name = infos[0];
		var type = infos[1];
		var Hp = infos[2];
		var Str = infos[3];
		var Intel = infos[4];
		var Agil = infos[5];
		var Def = infos[6];
		var Speed = infos[7];
		var url = CE.config.api.CLASS + "?name=" + type;
		var body = {};
		Requete.Requete([url, body], "GET", CE, function (response, body) {
			if (response.statusCode == 200) {
				var resp = JSON.parse(body);
				if (resp.length <= 0) {
					CE.socket.write("901" + "Error class id");
					return;
				}
				if (!resp[0].hasOwnProperty('_id')) {
					CE.socket.write("901" + "Error class id");
				}
				else {
					var idClass = resp[0]._id;
					var url = CE.config.api.CHAMPION;
					var body = JSON.stringify({ "name": name, "owner": CE.UID, "class": idClass,
					"speed": Speed,
					"strength": Str,
					"agility": Agil,
					"intel": Intel,
					"armor": Def,
					"healthpoint": Hp});
					Requete.Requete([url, body], "POST", CE, function (response, body) {
						if (response.statusCode == 200) {
							console.log(body);
							CE.socket.write("900" + body);
						}
						else {
							console.log(body);
							CE.socket.write("901" + "Error add champion \n" + body);
						}
					});
				}
			}
			else {
				CE.socket.write("901" + "Error class");
			}
		})
	}
	else {
		CE.socket.write("901" + "Error arguments");
	}

}

function RemoveChampion(data, CE) {
	var infos = data.split(/;|\n/);
	var id = infos[0];
	var url = CE.config.api.CHAMPION + "/" + id;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "DELETE", CE, function (response, body) {
		if (response.statusCode == 200) {
			CE.socket.write("902");
		}
		else
			CE.socket.write("903");
	})
}

function GetChampions(data, CE) {
	var infos = data.split(/;|\n/);
	var url = CE.config.api.CHAMPION + "?owner=" + CE.UID;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var ret = JSON.parse(body);
			var list = [];
			var count = 0;
			if (ret.length == 0) {
				CE.socket.write("904" + JSON.stringify(list));
			}
			else {
				for (var i = 0, len = ret.length; i < len; i++) {
					(function (i) {
						var url = CE.config.api.CLASS + "/" + ret[i].class;
						var body = JSON.stringify({});
						Requete.Requete([url, body], "GET", CE, function (response, body) {
							if (response.statusCode == 200) {
								var resp = JSON.parse(body);
								var ClassName = resp.name;
								var ClassDescription = resp.description;
								console.log(ClassName);
								console.log(ClassDescription);
								list.push(_.extend(ret[i], { "className": ClassName, "classDescription": ClassDescription }));
								count++;
								if (count == len) {
									CE.socket.write("904" + JSON.stringify(list));
								}
							}
							else
								CE.socket.write("905" + "Error get class champion");
						});
					})(i);
				}
			}
		}
		else
			CE.socket.write("905" + "Error get champion");
	});
}

function GetAllChampions(data, CE) {
	var infos = data.split(/;|\n/);
	var url = CE.config.api.CHAMPION;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var list = body;
			if (response.statusCode == 200)
				CE.socket.write("906" + list);
		}
		else
			CE.socket.write("907");
	});
}

function AddClass(data, CE) {
	var infos = data.split(/;|\n/);
	var name = infos[0];
	var url = CE.config.api.CLASS;
	var body = JSON.stringify({ "name": name });
	Requete.Requete([url, body], "POST", CE, function (response, body) {
		if (response.statusCode == 200)
			CE.socket.write("910");
		else
			CE.socket.write("911");
	})
}

function GetClass(data, CE) {
	var infos = data.split(/;|\n/);
	var url = CE.config.api.CLASS;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200)
			CE.socket.write("912" + body);
		else
			CE.socket.write("913");
	})
}


///
//
// Check if stuff is in inventory
// Get type
// Check if this stuff go to the Hand
// Put stuffID to champion stuff_hand
// Put is_use to stuff
//
///
/*function AddStuffHand(data, CE) {
	var infos = data.split(/;|\n/);
	var hand = infos[0];
	var idchampion = infos[1];
	var idstuff = infos[2];
	var stuff;
	if (hand == "right")
		stuff = { "stuff_hand_left": idstuff };
	else if (hand == "left")
		stuff = { "stuff_hand_right": idstuff };
	else {
		CE.socket.write("909" + "Error hand's select");
		return;
	}
	var url = CE.config.api.GETINVENTORY + "/" + idstuff;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var type = JSON.parse(body);
			var url = CE.config.api.GETTYPEINVENTORY + "/" + type.type;
			var body = JSON.stringify({});
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var bonus = resp.Bonus;
					console.log("Resp == " + body);
					var location = resp.location;
					if (location == "Two Hands" || location == "Left Hand" || location == "Right Hand") {

						var url = CE.config.api.CHAMPION + "/" + idchampion;
						Requete.Requete([url, body], "GET", CE, function (response, body) {
							if (response.statusCode == 200) {
								var resp = JSON.parse(body);
								var speed = resp.speed;
								var strength = resp.strength;
								var agility = resp.agility;
								var intel = resp.intel;
								var armor = resp.armor;
								var spellpower = resp.spellpower;
								var healthpoint = resp.healthpoint;
								var spellpoint = resp.spellpoint;
								var infos = bonus.split(/ |\n/);
								var action = infos[0];
								switch (action) {
									case "defense":
										var Iarmor = parseInt(armor) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "armor": Iarmor }, idchampion, "908", "909", true);
										break;
									case "strength":
										var Istrength = parseInt(strength) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "strength": Istrength }, idchampion, "908", "909", true);
										break;
									case "spellpower":
										var Ispellpower = parseInt(spellpower) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "spellpower": Ispellpower }, idchampion, "908", "909", true);
										break;
									case "lifepoint":
										var Ihealthpoint = parseInt(healthpoint) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "healthpoint": Ihealthpoint }, idchampion, "908", "909", true);
										break;
									case "intel":
										var Iintel = parseInt(intel) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "intel": Iintel }, idchampion, "908", "909", true);
										break;
									case "agility":
										var Iagility = parseInt(agility) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "agility": Iagility }, idchampion, "908", "909", true);
										break;
									case "speed":
										var Ispeed = parseInt(speed) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "speed": Ispeed }, idchampion, "908", "909", true);
										break;
									default:
										CE.socket.write("909" + "Error switch");
								}
								console.log(infos[0]);
								console.log(infos[1]);
								console.log(infos[2]);
							}
							else {
								CE.socket.write("909" + "Error champion");
							}
						})
					}
					else {
						CE.socket.write("909" + "Error not for hands");
					}
				}
				else
					CE.socket.write("909" + "Error Get Typestuff");
			});

		}
		else
			CE.socket.write("909" + "No stuff with this ID");
	})
}

function PutInfo(CE, stuff, idstuff, data, idchampion, codeOK, codeKO, IsUse) {
	console.log("PUTINFO = " + data);
	var body = _.extend(stuff, data);
	body = JSON.stringify(body);
	var url = CE.config.api.CHAMPION + "/" + idchampion;
	console.log("PUTINFO URL = " + url);
	Requete.Requete([url, body], "PUT", CE, function (response, body) {
		if (response.statusCode == 200) {
			var url = CE.config.api.GETINVENTORY + "/" + idstuff;
			console.log("PUTINFO URL Get = " + url);
			var body = JSON.stringify({ "is_use": IsUse });
			Requete.Requete([url, body], "PUT", CE, function (response, body) {
				if (response.statusCode == 200) {
					CE.socket.write(codeOK);
				}
				else
					CE.socket.write(codeKO + "Error put stuff in is_use");
			});
		}
		else
			CE.socket.write(codeKO + "Error put stuff id to idchampion");
	})
}

function AddStuffBody(data, CE) {
	var infos = data.split(/;|\n/);
	var idchampion = infos[0];
	var idstuff = infos[1];
	var idtype;
	var url = CE.config.api.GETINVENTORY + "/" + idstuff;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var type = JSON.parse(body);
			idtype = type.type;
			var url = CE.config.api.GETTYPEINVENTORY + "/" + idtype;
			var body = JSON.stringify({});
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var location = resp.location;
					var bonus = resp.Bonus;
					if (location == "Body") {
						var url = CE.config.api.CHAMPION + "/" + idchampion;
						Requete.Requete([url, body], "GET", CE, function (response, body) {
							if (response.statusCode == 200) {
								var resp = JSON.parse(body);
								var speed = resp.speed;
								var strength = resp.strength;
								var agility = resp.agility;
								var intel = resp.intel;
								var armor = resp.armor;
								var spellpower = resp.spellpower;
								var healthpoint = resp.healthpoint;
								var spellpoint = resp.spellpoint;
								var infos = bonus.split(/ |\n/);
								var stuff = { "stuff_body": idstuff };
								var action = infos[0];
								switch (action) {
									case "defense":
										var Iarmor = parseInt(armor) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "armor": Iarmor }, idchampion, "910", "911", true);
										break;
									case "strength":
										var Istrength = parseInt(strength) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "strength": Istrength }, idchampion, "910", "911", true);
										break;
									case "spellpower":
										var Ispellpower = parseInt(spellpower) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "spellpower": Ispellpower }, idchampion, "910", "911", true);
										break;
									case "lifepoint":
										var Ihealthpoint = parseInt(healthpoint) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "healthpoint": Ihealthpoint }, idchampion, "910", "911", true);
										break;
									case "intel":
										var Iintel = parseInt(intel) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "intel": Iintel }, idchampion, "910", "911", true);
										break;
									case "agility":
										var Iagility = parseInt(agility) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "agility": Iagility }, idchampion, "910", "911", true);
										break;
									case "speed":
										var Ispeed = parseInt(speed) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "speed": Ispeed }, idchampion, "910", "911", true);
										break;
									default:
										CE.socket.write("911" + "Error switch");
								}
								console.log(infos[0]);
								console.log(infos[1]);
								console.log(infos[2]);
							}
							else {
								CE.socket.write("911" + "Error champion");
							}
						})
					}
					else {
						CE.socket.write("911" + "Error not for body");
					}
				}
				else
					CE.socket.write("911" + "Error Get Typestuff");
			});

		}
		else
			CE.socket.write("911" + "No stuff with this ID");
	})
}

function AddStuffHead(data, CE) {
	var infos = data.split(/;|\n/);
	var idchampion = infos[0];
	var idstuff = infos[1];
	var url = CE.config.api.GETINVENTORY + "/" + idstuff;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var type = JSON.parse(body);
			var url = CE.config.api.GETTYPEINVENTORY + "/" + type.type;
			var body = JSON.stringify({});
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var location = resp.location;
					var bonus = resp.Bonus;
					if (location == "Head") {
						var url = CE.config.api.CHAMPION + "/" + idchampion;
						Requete.Requete([url, body], "GET", CE, function (response, body) {
							if (response.statusCode == 200) {
								var resp = JSON.parse(body);
								var speed = resp.speed;
								var strength = resp.strength;
								var agility = resp.agility;
								var intel = resp.intel;
								var armor = resp.armor;
								var spellpower = resp.spellpower;
								var healthpoint = resp.healthpoint;
								var spellpoint = resp.spellpoint;
								var infos = bonus.split(/ |\n/);
								var stuff = { "stuff_head": idstuff };
								var action = infos[0];
								switch (action) {
									case "defense":
										var Iarmor = parseInt(armor) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "armor": Iarmor }, idchampion, "912", "913", true);
										break;
									case "strength":
										var Istrength = parseInt(strength) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "strength": Istrength }, idchampion, "912", "913", true);
										break;
									case "spellpower":
										var Ispellpower = parseInt(spellpower) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "spellpower": Ispellpower }, idchampion, "912", "913", true);
										break;
									case "lifepoint":
										var Ihealthpoint = parseInt(healthpoint) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "healthpoint": Ihealthpoint }, idchampion, "912", "913", true);
										break;
									case "intel":
										var Iintel = parseInt(intel) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "intel": Iintel }, idchampion, "912", "913", true);
										break;
									case "agility":
										var Iagility = parseInt(agility) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "agility": Iagility }, idchampion, "912", "913", true);
										break;
									case "speed":
										var Ispeed = parseInt(speed) + parseInt(infos[2]);
										PutInfo(CE, stuff, idstuff, { "speed": Ispeed }, idchampion, "912", "913", true);
										break;
									default:
										CE.socket.write("913" + "Error switch");
								}
								console.log(infos[0]);
								console.log(infos[1]);
								console.log(infos[2]);
							}
							else {
								CE.socket.write("913" + "Error champion");
							}
						})
					}
					else {
						CE.socket.write("913" + "Error not for head");
					}
				}
				else
					CE.socket.write("913" + "Error Get Typestuff");
			});

		}
		else
			CE.socket.write("913" + "No stuff with this ID");
	})
}

function RemoveStuffHand(data, CE) {
	var infos = data.split(/;|\n/);
	var hand = infos[0];
	var idchampion = infos[1];
	var idstuff;
	var stuff;
	if (hand == "right")
		stuff = { "stuff_hand_right": null };
	else if (hand == "left")
		stuff = { "stuff_hand_left": null };
	else {
		CE.socket.write("909" + "Error hand's select");
		return;
	}
	var body;
	var url = CE.config.api.CHAMPION + "/" + idchampion;
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var resp = JSON.parse(body);
			var speed = resp.speed;
			var strength = resp.strength;
			var agility = resp.agility;
			var intel = resp.intel;
			var armor = resp.armor;
			var spellpower = resp.spellpower;
			var healthpoint = resp.healthpoint;
			var spellpoint = resp.spellpoint;
			if (hand == "right")
				var idstuff = resp.stuff_hand_right;
			else
				var idstuff = resp.stuff_hand_left;
			var url = CE.config.api.GETINVENTORY + "/" + idstuff
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var type = resp.type;
					var url = CE.config.api.GETTYPEINVENTORY + "/" + type;
					var body = JSON.stringify({});
					Requete.Requete([url, body], "GET", CE, function (response, body) {
						if (response.statusCode == 200) {
							var resp = JSON.parse(body);
							var bonus = resp.Bonus;
							var infos = bonus.split(/ |\n/);
							var action = infos[0];
							console.log("Action : " + action);
							switch (action) {
								case "defense":
									var Iarmor = parseInt(armor) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "armor": Iarmor }, idchampion, "914", "915", false);
									break;
								case "strength":
									var Istrength = parseInt(strength) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "strength": Istrength }, idchampion, "914", "915", false);
									break;
								case "spellpower":
									var Ispellpower = parseInt(spellpower) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "spellpower": Ispellpower }, idchampion, "914", "915", false);
									break;
								case "lifepoint":
									var Ihealthpoint = parseInt(healthpoint) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "healthpoint": Ihealthpoint }, idchampion, "914", "915", false);
									break;
								case "intel":
									var Iintel = parseInt(intel) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "intel": Iintel }, idchampion, "914", "915", false);
									break;
								case "agility":
									var Iagility = parseInt(agility) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "agility": Iagility }, idchampion, "914", "915", false);
									break;
								case "speed":
									var Ispeed = parseInt(speed) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "speed": Ispeed }, idchampion, "914", "915", false);
									break;
								default:
									CE.socket.write("915" + "Error switch");
							}
							console.log(infos[0]);
							console.log(infos[1]);
							console.log(infos[2]);
						}
						else {
							CE.socket.write("915" + "Error type");
						}
					});
				}
				else {
					CE.socket.write("915" + "Error stuff");
				}
			});
		}
		else {
			CE.socket.write("915" + "Error champion");
		}
	});
}

function RemoveStuffBody(data, CE) {
	var infos = data.split(/;|\n/);
	var idchampion = infos[0];
	var idstuff;
	var stuff = { "stuff_body": null };
	var body;
	var url = CE.config.api.CHAMPION + "/" + idchampion;
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var resp = JSON.parse(body);
			var speed = resp.speed;
			var strength = resp.strength;
			var agility = resp.agility;
			var intel = resp.intel;
			var armor = resp.armor;
			var spellpower = resp.spellpower;
			var healthpoint = resp.healthpoint;
			var spellpoint = resp.spellpoint;
			var idstuff = resp.stuff_body;
			var url = CE.config.api.GETINVENTORY + "/" + idstuff
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var type = resp.type;
					var url = CE.config.api.GETTYPEINVENTORY + "/" + type;
					var body = JSON.stringify({});
					Requete.Requete([url, body], "GET", CE, function (response, body) {
						if (response.statusCode == 200) {
							var resp = JSON.parse(body);
							var bonus = resp.Bonus;
							var infos = bonus.split(/ |\n/);
							var action = infos[0];
							console.log("Action : " + action);
							switch (action) {
								case "defense":
									var Iarmor = parseInt(armor) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "armor": Iarmor }, idchampion, "914", "915", false);
									break;
								case "strength":
									var Istrength = parseInt(strength) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "strength": Istrength }, idchampion, "914", "915", false);
									break;
								case "spellpower":
									var Ispellpower = parseInt(spellpower) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "spellpower": Ispellpower }, idchampion, "914", "915", false);
									break;
								case "lifepoint":
									var Ihealthpoint = parseInt(healthpoint) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "healthpoint": Ihealthpoint }, idchampion, "914", "915", false);
									break;
								case "intel":
									var Iintel = parseInt(intel) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "intel": Iintel }, idchampion, "914", "915", false);
									break;
								case "agility":
									var Iagility = parseInt(agility) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "agility": Iagility }, idchampion, "914", "915", false);
									break;
								case "speed":
									var Ispeed = parseInt(speed) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "speed": Ispeed }, idchampion, "914", "915", false);
									break;
								default:
									CE.socket.write("915" + "Error switch");
							}
							console.log(infos[0]);
							console.log(infos[1]);
							console.log(infos[2]);
						}
						else {
							CE.socket.write("915" + "Error type");
						}
					});
				}
				else {
					CE.socket.write("915" + "Error stuff");
				}
			});
		}
		else {
			CE.socket.write("915" + "Error champion");
		}
	});
}

function RemoveStuffHead(data, CE) {
	var infos = data.split(/;|\n/);
	var idchampion = infos[0];
	var idstuff;
	var stuff = { "stuff_head": null };
	var body;
	var url = CE.config.api.CHAMPION + "/" + idchampion;
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var resp = JSON.parse(body);
			var speed = resp.speed;
			var strength = resp.strength;
			var agility = resp.agility;
			var intel = resp.intel;
			var armor = resp.armor;
			var spellpower = resp.spellpower;
			var healthpoint = resp.healthpoint;
			var spellpoint = resp.spellpoint;
			var idstuff = resp.stuff_head;
			var url = CE.config.api.GETINVENTORY + "/" + idstuff
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var type = resp.type;
					var url = CE.config.api.GETTYPEINVENTORY + "/" + type;
					var body = JSON.stringify({});
					Requete.Requete([url, body], "GET", CE, function (response, body) {
						if (response.statusCode == 200) {
							var resp = JSON.parse(body);
							var bonus = resp.Bonus;
							var infos = bonus.split(/ |\n/);
							var action = infos[0];
							console.log("Action : " + action);
							switch (action) {
								case "defense":
									var Iarmor = parseInt(armor) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "armor": Iarmor }, idchampion, "914", "915", false);
									break;
								case "strength":
									var Istrength = parseInt(strength) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "strength": Istrength }, idchampion, "914", "915", false);
									break;
								case "spellpower":
									var Ispellpower = parseInt(spellpower) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "spellpower": Ispellpower }, idchampion, "914", "915", false);
									break;
								case "lifepoint":
									var Ihealthpoint = parseInt(healthpoint) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "healthpoint": Ihealthpoint }, idchampion, "914", "915", false);
									break;
								case "intel":
									var Iintel = parseInt(intel) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "intel": Iintel }, idchampion, "914", "915", false);
									break;
								case "agility":
									var Iagility = parseInt(agility) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "agility": Iagility }, idchampion, "914", "915", false);
									break;
								case "speed":
									var Ispeed = parseInt(speed) - parseInt(infos[2]);
									PutInfo(CE, stuff, idstuff, { "speed": Ispeed }, idchampion, "914", "915", false);
									break;
								default:
									CE.socket.write("915" + "Error switch");
							}
							console.log(infos[0]);
							console.log(infos[1]);
							console.log(infos[2]);
						}
						else {
							CE.socket.write("915" + "Error type");
						}
					});
				}
				else {
					CE.socket.write("915" + "Error with stuff");
				}
			});
		}
		else {
			CE.socket.write("915" + "Error champion");
		}
	});
}*/

function PutEffect(CE, data, idchampion, ok, ko) {
	var body = data;
	body = JSON.stringify(body);
	var url = CE.config.api.CHAMPION + "/" + idchampion;
	console.log("PUTINFO URL = " + url);
	Requete.Requete([url, body], "PUT", CE, function (response, body) {
		if (response.statusCode == 200) {
			CE.socket.write(ok);
		}
		else {
			CE.socket.write(ko + "Error evolution champion");
		}
	});
}

function AddStuffEffect(CE, idchampion, idstuff, operator, ok, ko) {
	var url = CE.config.api.GETINVENTORY + "/" + idstuff;
	var body = JSON.stringify({});
	Requete.Requete([url, body], "GET", CE, function (response, body) {
		if (response.statusCode == 200) {
			var type = JSON.parse(body);
			var url = CE.config.api.GETTYPEINVENTORY + "/" + type.type;
			var body = JSON.stringify({});
			Requete.Requete([url, body], "GET", CE, function (response, body) {
				if (response.statusCode == 200) {
					var resp = JSON.parse(body);
					var bonus = resp.Bonus;
					var url = CE.config.api.CHAMPION + "/" + idchampion;
					Requete.Requete([url, body], "GET", CE, function (response, body) {
						if (response.statusCode == 200) {
							var resp = JSON.parse(body);
							var speed = resp.speed;
							var strength = resp.strength;
							var agility = resp.agility;
							var intel = resp.intel;
							var armor = resp.armor;
							var spellpower = resp.spellpower;
							var healthpoint = resp.healthpoint;
							var spellpoint = resp.spellpoint;
							var infos = bonus.split(/ |\n/);
							var action = infos[0];
							if (operator == "+")
								switch (action) {
									case "defense":
										var Iarmor = parseInt(armor) + parseInt(infos[2]);
										PutEffect(CE, { "armor": Iarmor }, idchampion, ok, ko);
										break;
									case "strength":
										var Istrength = parseInt(strength) + parseInt(infos[2]);
										PutEffect(CE, { "strength": Istrength }, idchampion, ok, ko);
										break;
									case "spellpower":
										var Ispellpower = parseInt(spellpower) + parseInt(infos[2]);
										PutEffect(CE, { "spellpower": Ispellpower }, idchampion, ok, ko);
										break;
									case "lifepoint":
										var Ihealthpoint = parseInt(healthpoint) + parseInt(infos[2]);
										PutEffect(CE, { "healthpoint": Ihealthpoint }, idchampion, ok, ko);
										break;
									case "intel":
										var Iintel = parseInt(intel) + parseInt(infos[2]);
										PutEffect(CE, { "intel": Iintel }, idchampion, ok, ko);
										break;
									case "agility":
										var Iagility = parseInt(agility) + parseInt(infos[2]);
										PutEffect(CE, { "agility": Iagility }, idchampion, ok, ko);
										break;
									case "speed":
										var Ispeed = parseInt(speed) + parseInt(infos[2]);
										PutEffect(CE, { "speed": Ispeed }, idchampion, ok, ko);
										break;
									default:
										CE.socket.write(ko + "Error switch");
								}
							else
								switch (action) {
									case "defense":
										var Iarmor = parseInt(armor) - parseInt(infos[2]);
										PutEffect(CE, { "armor": Iarmor }, idchampion, ok, ko);
										break;
									case "strength":
										var Istrength = parseInt(strength) - parseInt(infos[2]);
										PutEffect(CE, { "strength": Istrength }, idchampion, ok, ko);
										break;
									case "spellpower":
										var Ispellpower = parseInt(spellpower) - parseInt(infos[2]);
										PutEffect(CE, { "spellpower": Ispellpower }, idchampion, ok, ko);
										break;
									case "lifepoint":
										var Ihealthpoint = parseInt(healthpoint) - parseInt(infos[2]);
										PutEffect(CE, { "healthpoint": Ihealthpoint }, idchampion, ok, ko);
										break;
									case "intel":
										var Iintel = parseInt(intel) - parseInt(infos[2]);
										PutEffect(CE, { "intel": Iintel }, idchampion, ok, ko);
										break;
									case "agility":
										var Iagility = parseInt(agility) - parseInt(infos[2]);
										PutEffect(CE, { "agility": Iagility }, idchampion, ok, ko);
										break;
									case "speed":
										var Ispeed = parseInt(speed) - parseInt(infos[2]);
										PutEffect(CE, { "speed": Ispeed }, idchampion, ok, ko);
										break;
									default:
										CE.socket.write(ko + "Error switch");
								}
						}
						else {
							CE.socket.write(ko + "Error champion");
						}
					});
				}
				else {
					CE.socket.write(ko + "Error type");
				}
			});
		}
		else {
			CE.socket.write(ko + "Error stuff");
		}
	});
}

function AddStuff(data, CE) {
	var infos = data.split(/;/);
	console.log(infos);
	if (infos.length != 2) {
		CE.socket.write("909" + "Error arguments");
	}
	else {
		var idchampion = infos[0];
		var idstuff = infos[1];
		var url = CE.config.api.GETINVENTORY + "/" + idstuff;
		console.log("PUTINFO URL Get = " + url);
		var body = JSON.stringify({ "is_use": true, "champion": idchampion });
		Requete.Requete([url, body], "PUT", CE, function (response, body) {
			if (response.statusCode == 200) {
				AddStuffEffect(CE, idchampion, idstuff, "+", "908", "909");
			}
			else
				CE.socket.write("909" + "Error put stuff in is_use");
		});
	}
}

function RemoveStuff(data, CE) {
	var infos = data.split(/;/);
	if (infos.length != 2) {
		CE.socket.write("911" + "Error arguments");
	}
	else {
		var idchampion = infos[0];
		var idstuff = infos[1];
		var url = CE.config.api.GETINVENTORY + "/" + idstuff;
		console.log("PUTINFO URL Get = " + url);
		var body = JSON.stringify({ "is_use": false, "champion": null });
		Requete.Requete([url, body], "PUT", CE, function (response, body) {
			if (response.statusCode == 200) {
				AddStuffEffect(CE, idchampion, idstuff, "-", "910", "911");
			}
			else
				CE.socket.write("911" + "Error put stuff in is_use");
		});
	}
}

exports.GetChampions = GetChampions;
exports.RemoveChampion = RemoveChampion;
exports.AddChampion = AddChampion;
exports.GetAllChampions = GetAllChampions;
exports.AddClass = AddClass;
exports.GetClass = GetClass;
/*exports.AddStuffHand = AddStuffHand;
exports.AddStuffBody = AddStuffBody;
exports.AddStuffHead = AddStuffHead;
exports.RemoveStuffHand = RemoveStuffHand;
exports.RemoveStuffBody = RemoveStuffBody;
exports.RemoveStuffHead = RemoveStuffHead;*/
exports.AddStuff = AddStuff;
exports.RemoveStuff = RemoveStuff;