var request = require('request');

function FirstRequete(rep, CE, callback) {
    request.post({
        headers: { 'Content-Type': 'application/json' },
        url: rep[0],
        body: rep[1]
    }, function (error, response, body) {
        if (error) {
            CE.socket.write("500");
            return console.log(error);
        }
            callback(response, body)
    });
};

function Requete(rep, type, CE, callback) {
    if (type == "POST") {
        request.post({
            headers: { 'Content-Type': 'application/json', 'Authorization': CE.Token },
            url: rep[0],
            body: rep[1]
        }, function (error, response, body) {
            if (error) {
                CE.socket.write("500");
                return console.log(error);
            }
            callback(response, body)
        });
    }
    else if (type == "GET") {
        console.log(rep[0]);
        request.get({
            headers: { 'Content-Type': 'application/json', 'Authorization': CE.Token },
            url: rep[0]
        }, function (error, response, body) {
            if (error) {
                CE.socket.write("500");
                return console.log(error);
            }
            callback(response, body)
        });
    }
    else if (type == "PUT") {
        console.log("Requete PUT url " + rep[0]);
        console.log("Requete PUT body " + rep[1]);
        request.put({
            headers: { 'Content-Type': 'application/json', 'Authorization': CE.Token },
            url: rep[0],
            body: rep[1]
        }, function (error, response, body) {
            if (error) {
                CE.socket.write("500");
                return console.log(error);
            }
            callback(response, body)
        });
    }
    else if (type == "DELETE") {
        request.del({
            headers: { 'Content-Type': 'application/json', 'Authorization': CE.Token },
            url: rep[0]
        }, function (error, response, body) {
            if (error) {
                CE.socket.write("500");
                return console.log(error);
            }
            callback(response, body)
        });
    }
};

exports.FirstRequete = FirstRequete;
exports.Requete = Requete;