var Requete = require('./Requete')
var app = require('./app')
var _ = require('underscore');

function GetInventory(data, CE) {
    var url = CE.config.api.ITEM + "?owner=" + CE.UID;
    var Mainbody = JSON.stringify({});
    var array = [];
    var count = 0;
    Requete.Requete([url, Mainbody], "GET", CE, function (response, Mainbody) {
        if (response.statusCode == 200) {
            var ret = JSON.parse(Mainbody);
            var url = CE.config.api.GETTYPEINVENTORY;
            if (ret.length == 0) {
                CE.socket.write("800" + JSON.stringify([]));
            }
            else {
                for (var i = 0, len = ret.length; i < len; i++) {
                    (function (i) {
                        var name = ret[i].name;
                        var type = ret[i].type;
                        var owner = ret[i].owner;
                        var id = ret[i]._id;
                        var is_use = ret[i].is_use;
                        var champion = null;
                        if (ret[i].hasOwnProperty('champion'))
                            champion = ret[i].champion;
                        var body = JSON.stringify({});
                        Requete.Requete([url + "/" + type, body], "GET", CE, function (response, body) {
                            if (response.statusCode == 200) {
                                var tab = JSON.parse(body);
                                var location = tab.location;
                                var Bonus = tab.Bonus;
                                var sprite = tab.sprite;
                                body = ({ "name": name, "type": type, "owner": owner,"Bonus":Bonus, "_id": id, "is_use": is_use, "location": location, "sprite": sprite, "champion": champion });
                                array.push(body);
                                count++;
                                if (count == len) {
                                    CE.socket.write("800" + JSON.stringify(array));
                                }
                            }
                            else
                                CE.socket.write("801");
                        });
                    })(i)
                }
            }
        }
        else
            CE.socket.write("801");
    })
}

function GetInventoryUsable(data, CE) {
    var url = CE.config.api.GETUSABLE + "?owner=" + CE.UID;
    var Mainbody = JSON.stringify({});
    var array = [];
    var count = 0;
    Requete.Requete([url, Mainbody], "GET", CE, function (response, Mainbody) {
        if (response.statusCode == 200) {
            var ret = JSON.parse(Mainbody);
            var url = CE.config.api.GETTYPEUSABLE;
            if (ret.length == 0) {
                CE.socket.write("800" + JSON.stringify([]));
            }
            else {
                for (var i = 0, len = ret.length; i < len; i++) {
                    (function (i) {
                        var type = ret[i].item_type;
                        var owner = ret[i].owner;
                        var id = ret[i]._id;
                        var quantity = ret[i].quantity;
                        var body = JSON.stringify({});
                        Requete.Requete([url + "/" + type, body], "GET", CE, function (response, body) {
                            if (response.statusCode == 200) {
                                var tab = JSON.parse(body);
                                var name = tab.name;
                                var effect = tab.effect;
                                var price = tab.price;
                                var sprite = tab.sprite;
                                body = ({ "name": name, "type": type, "quantity":quantity, "owner": owner, "_id": id, "sprite": sprite, "effect":effect, "price":price });
                                array.push(body);
                                count++;
                                if (count == len) {
                                    CE.socket.write("800" + JSON.stringify(array));
                                }
                            }
                            else
                                CE.socket.write("801");
                        });
                    })(i)
                }
            }
        }
        else
            CE.socket.write("801");
    })
}

function GetTypeInventory(data, CE) {
    var url = CE.config.api.GETTYPEINVENTORY;
    var body = JSON.stringify({});
    Requete.Requete([url, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            CE.socket.write("820" + body);
        }
        else {
            console.log("Error == " + body);
            CE.socket.write("821");
        }
    })
}

function GetTypeUsable(data, CE) {
    var url = CE.config.api.GETTYPEUSABLE;
    var body = JSON.stringify({});
    Requete.Requete([url, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            CE.socket.write("820" + body);
        }
        else
            CE.socket.write("821");
    })
}

function SellObject(data, CE) {
    /* var infos = data.split(/;|\n/);
     var idobject = infos[0];
     var quantity = infos[1];
     var url = CE.config.api.SELLOBJECT;
     var body = JSON.stringify({ "id_object": idobject, "quantity": quantity });
     Requete.Requete([url, body], "POST", CE, function (response, body) {
         if (response.statusCode == 200) {
             CE.socket.write("802");
         }
         else
             CE.socket.write("803");
     })*/
}

function UseItem(data, CE) {
    var infos = data.split(/;|\n/);
    var idobject = infos[0];
    var url = CE.config.api.ITEM + "/" + idobject;
    var body = JSON.stringify({});
    Requete.Requete([url, body], "DELETE", CE, function (response, body) {
        if (response.statusCode == 200) {
            CE.socket.write("804");
        }
        else
            CE.socket.write("805");
    })
}

function UseUsable(data, CE) {
    var infos = data.split(/;|\n/);
    var type = infos[0];
    var quantity = infos[1];
    var url = CE.config.api.GETUSABLE;
    var body = JSON.stringify();
    Requete.Requete([url + "?owner=" + CE.UID, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            var array = JSON.parse(body);
            var i = 0;
            function loop(i) {
                console.log(" size == " + array.length);
                console.log("i ==== " + i);
                if (array[i].item_type == type) {
                    if (isNaN(parseInt(array[i].quantity)))
                        array[i].quantity = 0;
                    if (isNaN(parseInt(quantity)))
                        quantity = 0;
                    var body = JSON.stringify({ "quantity": parseInt(array[i].quantity) - parseInt(quantity) });
                    Requete.Requete([url + "/" + array[i]._id, body], "PUT", CE, function (response, body) {
                        if (response.statusCode == 200) {
                            CE.socket.write("804");
                        }
                        else {
                            console.log(body);
                            CE.socket.write("805");
                        }
                    });
                }
                else {
                    if (i < array.length - 1) {
                        i = i + 1;
                        loop(i);
                    }
                    else {
                        CE.socket.write("805");
                    }
                }

            }
            loop(i);
        }
        else
            CE.socket.write("805");
    });
}

function AddItem(data, CE) {
    var infos = data.split(/;|\n/);
    var type = infos[0];
    var url = CE.config.api.GETTYPEINVENTORY;
    var body = JSON.stringify({});
    var name = "";
    Requete.Requete([url + "?_id=" + type, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            var array = JSON.parse(body);
            name = array[0].name;
            var url = CE.config.api.ITEM;
            var body = JSON.stringify({ "name": name, "type": type, "owner": CE.UID });
            console.log(url);
            Requete.Requete([url, body], "POST", CE, function (response, body) {
                if (response.statusCode == 200) {
                    CE.socket.write("806");
                }
                else {
                    CE.socket.write("807");
                }
            })
        }
        else {
            CE.socket.write("807");
        }
    })

}

function AddUsable(data, CE) {
    var infos = data.split(/;|\n/);
    var type = infos[0];
    var quantity = infos[1];
    var url = CE.config.api.GETUSABLE;
    var body = JSON.stringify();
    Requete.Requete([url + "?owner=" + CE.UID, body], "GET", CE, function (response, body) {
        if (response.statusCode == 200) {
            var array = JSON.parse(body);
            var i = 0;
            if (array.length > 0) {
                function loop(i) {
                    console.log("SERVEUR = " + parseInt(array[i].quantity));
                    console.log("CLIENT = " + parseInt(quantity));
                    if (array[i].item_type == type) {
                        if (isNaN(parseInt(array[i].quantity)))
                            array[i].quantity = 0;
                        if (isNaN(parseInt(quantity)))
                            quantity = 0;
                        var body = JSON.stringify({ "quantity": parseInt(quantity) + parseInt(array[i].quantity) });
                        Requete.Requete([url + "/" + array[i]._id, body], "PUT", CE, function (response, body) {
                            if (response.statusCode == 200) {
                                CE.socket.write("806");
                            }
                            else {
                                console.log(body);
                                CE.socket.write("807");
                            }
                        });
                    }
                    else {
                        if (i < array.length - 1) {
                            i = i + 1;
                            loop(i);
                        }
                        else {
                            var body = JSON.stringify({ "item_type": type, "owner": CE.UID, "quantity": quantity });
                            Requete.Requete([url, body], "POST", CE, function (response, body) {
                                if (response.statusCode == 200) {
                                    CE.socket.write("806");
                                }
                                else {
                                    console.log(body);
                                    CE.socket.write("807");
                                }
                            })
                        }
                    }

                }
                loop(i);
            }
            else {
                var body = JSON.stringify({ "item_type": type, "owner": CE.UID, "quantity": quantity });
                Requete.Requete([url, body], "POST", CE, function (response, body) {
                    if (response.statusCode == 200) {
                        CE.socket.write("806");
                    }
                    else {
                        console.log(body);
                        CE.socket.write("807");
                    }
                })
            }
        }
        else
            CE.socket.write("807");
    })
}

exports.GetInventory = GetInventory;
exports.SellObject = SellObject;
exports.GetInventoryUsable = GetInventoryUsable;
exports.UseItem = UseItem;
exports.UseUsable = UseUsable;
exports.AddItem = AddItem;
exports.AddUsable = AddUsable;
exports.GetTypeInventory = GetTypeInventory;
exports.GetTypeUsable = GetTypeUsable;