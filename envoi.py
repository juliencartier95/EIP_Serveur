#! /usr/bin/env python
import time
import sys

if len(sys.argv) != 3:
        sys.stderr.write("mauvais nombre d'arguments.\n");
        sys.stderr.write("Syntaxe :\n");
        sys.stderr.write("envoi.py login nb_msg\n");
        sys.exit(1)

print "102toto;tutu"
sys.stdout.flush()
time.sleep(3)
nb_msg = int(sys.argv[2])
for i in range(nb_msg):
        print "201";
        sys.stdout.flush();
        time.sleep(0.01)
print "quit"